import ServiceInput from 'mitsi_bot_lib/dist/src/ServiceInput';
import { Response, Request, Application } from "express";
import BaseService from 'mitsi_bot_lib/dist/src/BaseService';
import TelegramApi from "./TelegramApi";

export class Server extends BaseService {
    telegramClient: TelegramApi;

    constructor(app: Application, client: TelegramApi) {
        super(app);
        this.telegramClient = client;
    }

    onPost(request: Request, response: Response): void {
        const serviceInput = request.body as ServiceInput;
        this.telegramClient.send(serviceInput.parameters);
        response.sendStatus(204);
    }
}