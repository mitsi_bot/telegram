import express from 'express';
import { Server } from "./server";
import TelegramApi from "./TelegramApi";
const config = require('config-yml');

const telegram = new TelegramApi(config.app.token as string, config.app.chatId as number, config.webservices.languageProcessingApi as string);
telegram.logInfo();
telegram.listen();
new Server(express(), telegram).startServer();
