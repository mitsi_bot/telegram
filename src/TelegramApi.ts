import axios, { AxiosResponse } from 'axios';
import Telegram, { GetUpdateMessageProvider } from 'telegram-bot-api';
import { Update, User } from 'telegram-typings'
import ResponseText from "mitsi_bot_lib/dist/src/ResponseText";
import LanguageProcessingApiInput from "mitsi_bot_lib/dist/src/LanguageProcessingApiInput";

export default class TelegramApi {
  private client: Telegram;
  private chatId: number;
  private languageProcessingUri: string;

  constructor(token: string, myChatId: number, languageProcessingUri: string) {
    this.chatId = myChatId;
    this.languageProcessingUri = languageProcessingUri;
    this.client = new Telegram({
      token,
      updates: {
        enabled: true
      }
    });
  }

  public listen(): void {
    this.client.setMessageProvider(new GetUpdateMessageProvider());
    this.client.start()
      .then(() => { console.log('Listening updates....') })
      .catch((e: Error) => console.error(e));

    this.client.on("update", (update: Update) => {
      const message = update.message;
      if (!message) {
        console.warn("Received update with empty message:", update);
        return;
      }
      const startDate = new Date();
      if (!message.text) {
        console.error("TELEGRAM", "Message from telegram api was undefined");
        this.client.sendMessage({ chat_id: message.chat.id, text: "Did not receive the content of the message :/" });
        return;
      }
      const data = new LanguageProcessingApiInput(message.text, message.chat.id === this.chatId);
      axios
        .post(this.languageProcessingUri, data)
        .then((response: AxiosResponse<ResponseText>) => {
          const endDate = new Date();
          console.log("TTR:", endDate.getTime() - startDate.getTime());
          this.client.sendMessage({
            chat_id: message.chat.id,
            text: response.data.displayText
          });
        })
        .catch((err: NodeJS.ErrnoException) => {
          console.error("TELEGRAM", err);          
          this.client.sendMessage({ chat_id: message.chat.id, text: `Error 😢: ${err.errno}` });
        });
    });
  }

  public send(message: string): void {
    this.client.sendMessage({
      chat_id: this.chatId,
      text: message
    });
  }

  public logInfo(): Promise<void> {
    return this.client.getMe()
      .then((user: User) => {
        console.info("getMe", user);
      })
      .catch((err: Error) => {
        console.error("getMe", err);
      });
  }
}