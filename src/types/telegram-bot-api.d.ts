declare module "telegram-bot-api" {
    import { User, Update } from 'telegram-typings';

    interface TelegramOptions {
        token: string,
        updates: {
            enabled: boolean
        }
    }

    export interface ChatResponse {
        chat_id: number,
        text: string
    }

    interface OnIntentCallback { (update: Update): void }

    export default class Telegram {
        constructor(options: TelegramOptions);
        on(intent: string, callback: OnIntentCallback): void;
        getMe(): Promise<User>;
        sendMessage(chatResponse: ChatResponse): void;
        listen(): void;
        setMessageProvider(any: any): void;
        start(): Promise<void>;
    }

    export class GetUpdateMessageProvider {
        constructor();
        start(): Promise<any>;
        stop(): Promise<any>;
    }
}
