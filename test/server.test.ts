import ServiceInput from 'mitsi_bot_lib/dist/src/ServiceInput';
import { Server } from '../src/server';
import express, { Request, Response } from 'express';
import TelegramApi from '../src/TelegramApi';

jest.mock('telegram-bot-api');
jest.mock('axios');
jest.mock('express');
const mockedTelegramApi = TelegramApi as jest.Mocked<typeof TelegramApi>;
const mockExpress = express as jest.Mocked<typeof express>;

describe("server", () => {
  it("should send message", () => {
    mockedTelegramApi.prototype.send = jest.fn()
    const server = new Server(mockExpress.application, mockedTelegramApi.prototype);
    const request = {
      body: new ServiceInput("plop"),
    } as Request;
    const NO_CONTENT = 204;
    const response = { sendStatus: jest.fn() } as any as Response;
    server.onPost(request, response);
    expect(mockedTelegramApi.prototype.send).toHaveBeenCalledWith("plop");
    expect(response.sendStatus).toHaveBeenCalledWith(NO_CONTENT);
  });
})

