import { Message, Chat, User, Update, CallbackQuery, ChosenInlineResult, InlineQuery, PreCheckoutQuery, ShippingQuery } from 'telegram-typings';

export class TelegramUpdate implements Update {
    constructor(chatId?: number, senderType?: string, content?: string) {
        this.update_id = -1;
        this.message = chatId && senderType ? new TelegramMessage(chatId, senderType, content) as Message : undefined;
    }
    update_id: number;
    message?: Message | undefined;
    edited_message?: Message | undefined;
    channel_post?: Message | undefined;
    edited_channel_post?: Message | undefined;
    inline_query?: InlineQuery | undefined;
    chosen_inline_result?: ChosenInlineResult | undefined;
    callback_query?: CallbackQuery | undefined;
    shipping_query?: ShippingQuery | undefined;
    pre_checkout_query?: PreCheckoutQuery | undefined;

}
class TelegramMessage implements Message {
    constructor(chatId: number, senderType: string, content?: string) {
        const telegramChat = new TelegramChat(chatId, senderType);
        this.text = content;
        this.chat = telegramChat as Chat;
        this.date = new Date().getTime();
        this.message_id = -1;
    }
    message_id: number;
    from?: import("telegram-typings").User | undefined;
    date: number;
    chat: import("telegram-typings").Chat;
    forward_from?: import("telegram-typings").User | undefined;
    forward_from_chat?: import("telegram-typings").Chat | undefined;
    forward_from_message_id?: number | undefined;
    forward_signature?: string | undefined;
    forward_date?: number | undefined;
    reply_to_message?: Message | undefined;
    edit_date?: number | undefined;
    media_group_id?: string | undefined;
    author_signature?: string | undefined;
    text?: string | undefined;
    entities?: import("telegram-typings").MessageEntity[] | undefined;
    caption_entities?: import("telegram-typings").MessageEntity[] | undefined;
    audio?: import("telegram-typings").Audio | undefined;
    document?: import("telegram-typings").Document | undefined;
    animation?: import("telegram-typings").Animation | undefined;
    game?: import("telegram-typings").Game | undefined;
    photo?: import("telegram-typings").PhotoSize[] | undefined;
    sticker?: import("telegram-typings").Sticker | undefined;
    video?: import("telegram-typings").Video | undefined;
    voice?: import("telegram-typings").Voice | undefined;
    video_note?: import("telegram-typings").VideoNote | undefined;
    caption?: string | undefined;
    contact?: import("telegram-typings").Contact | undefined;
    location?: import("telegram-typings").Location | undefined;
    venue?: import("telegram-typings").Venue | undefined;
    new_chat_members?: import("telegram-typings").User[] | undefined;
    left_chat_member?: import("telegram-typings").User | undefined;
    new_chat_title?: string | undefined;
    new_chat_photo?: import("telegram-typings").PhotoSize[] | undefined;
    delete_chat_photo?: true | undefined;
    group_chat_created?: true | undefined;
    supergroup_chat_created?: true | undefined;
    channel_chat_created?: true | undefined;
    migrate_to_chat_id?: number | undefined;
    migrate_from_chat_id?: number | undefined;
    pinned_message?: Message | undefined;
    invoice?: import("telegram-typings").Invoice | undefined;
    successful_payment?: import("telegram-typings").SuccessfulPayment | undefined;
    connected_website?: string | undefined;
    passport_data?: import("telegram-typings").PassportData | undefined;
}

class TelegramChat implements Chat {
    constructor(id: number, type: string) {
        this.id = id;
        this.type = type;
    }
    id: number;
    type: string;
    title?: string | undefined;
    username?: string | undefined;
    first_name?: string | undefined;
    last_name?: string | undefined;
    all_members_are_administrators?: boolean | undefined;
    photo?: import("telegram-typings").ChatPhoto | undefined;
    description?: string | undefined;
    invite_link?: string | undefined;
    pinned_message?: Message | undefined;
    sticker_set_name?: string | undefined;
    can_set_sticker_set?: boolean | undefined;
}

export class TelegramUser implements User {
    constructor() {
        this.id = 18;
        this.is_bot = true;
        this.first_name = "E.L.I.O.T";
    }
    id: number;
    is_bot: boolean;
    first_name: string;
    last_name?: string | undefined;
    username?: string | undefined;
    language_code?: string | undefined;
}