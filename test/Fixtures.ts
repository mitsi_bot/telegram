import { AxiosResponse } from "axios";
import ResponseText from "mitsi_bot_lib/dist/src/ResponseText";
import { Update } from "telegram-typings";
import { TelegramUpdate } from "./MockTypes";

export function axiosResponse(data: ResponseText, statusCode: number = 200): AxiosResponse<ResponseText> {
    return {
        data,
        status: statusCode,
        statusText: "OK",
        config: {},
        headers: {},
    }
};

export function axiosError(): any {
    return {
        errno: 'ECONNREFUSED',
        syscall: 'connect',
        port: 9001,
        request: {},
        isAxiosError: true,
        code: 'ECONNREFUSED',
        address: '127.0.0.1',
        config: {},
        response: undefined,
        toJSON: Function.prototype,
    }
};

export function forgeMyUserUpdate(): Update {
    return forgeMyUpdate("content");
}

export function forgeMyUpdate(message?: string): Update {
    return new TelegramUpdate(1124, "human", message);
}