import { axiosError, axiosResponse, forgeMyUpdate, forgeMyUserUpdate } from './Fixtures';
import { TelegramUpdate, TelegramUser } from './MockTypes';
import { User, Update } from 'telegram-typings';
import axios from "axios";
import ResponseText from "mitsi_bot_lib/dist/src/ResponseText";
import Telegram, { ChatResponse } from 'telegram-bot-api';
import TelegramApi from '../src/TelegramApi';

jest.mock('telegram-bot-api');
jest.mock('axios');
const mockedAxios = axios as jest.Mocked<typeof axios>;

describe("telegram", () => {

  beforeEach(() => {
    Telegram.prototype.start = jest.fn(() => Promise.resolve());
  });
  it("it should register listener", done => {
    Telegram.prototype.on = jest.fn().mockImplementationOnce((intentFilter, _callback) => {
      expect(intentFilter).toBe("update")
      done();
    });
    new TelegramApi("mytoken", 1124, "http://uri").listen();
  });

  it("it should call on process message not authenticated", done => {
    assertProcessMessage(new TelegramUpdate(123, "human", "content"), (uri: string, content: string, isAuthenticatd: boolean) => {
      expect(uri).toBe("http://uri");
      expect(content).toBe("content");
      expect(isAuthenticatd).toBe(false);
      done();
    });
    new TelegramApi("mytoken", 1124, "http://uri").listen();
  });

  it("it should call on process message authenticated", done => {
    assertProcessMessage(forgeMyUserUpdate(), (uri: string, content: string, isAuthenticatd: boolean) => {
      expect(uri).toBe("http://uri");
      expect(content).toBe("content");
      expect(isAuthenticatd).toBe(true);
      done();
    });
    new TelegramApi("mytoken", 1124, "http://uri").listen();
  });

  it("it should send user the response", done => {
    simulatebotResponse(true);
    simulateReceiveMessage(forgeMyUserUpdate());
    Telegram.prototype.sendMessage = jest.fn((response: ChatResponse) => {
      expect(response.chat_id).toBe(1124);
      expect(response.text).toBe("Success")
      done();
    });
    new TelegramApi("mytoken", 1124, "http://uri").listen();
  });

  it("it should send user the response if fail", done => {
    simulatebotResponse(false);
    simulateReceiveMessage(forgeMyUserUpdate());
    Telegram.prototype.sendMessage = jest.fn((response: ChatResponse) => {
      expect(response.chat_id).toBe(1124);
      expect(response.text).toBe("Error 😢: ECONNREFUSED");
      done();
    });
    new TelegramApi("mytoken", 1124, "http://uri").listen();
  });

  it("it should return an error if user message is not defined", done => {
    simulateReceiveMessage(forgeMyUpdate(undefined));
    Telegram.prototype.sendMessage = jest.fn((response: ChatResponse) => {
      expect(response.chat_id).toBe(1124);
      expect(response.text).toBe("Did not receive the content of the message :/")
      done();
    });
    new TelegramApi("mytoken", 1124, "http://uri").listen();
  });


  describe("logs", () => {
    const originalError = console.error;
    const originalInfo = console.info;
    const originalWarn = console.warn;

    beforeEach(() => {
      console.error = jest.fn();
      console.info = jest.fn();
      console.warn = jest.fn();
    });

    afterEach(() => {
      console.error = originalError;
      console.info = originalInfo;
      console.warn = originalWarn;
    });

    describe("update", () => {
      it("should warn and do nothing if message is undefined", () => {
        const update = new TelegramUpdate();
        simulateReceiveMessage(update);
        new TelegramApi("mytoken", 1124, "http://uri").listen();
        expect(console.warn).toHaveBeenCalledTimes(1);
      });
    });

    describe("getInfo", () => {
      it("it should log in error", done => {
        const error = new Error("an error has occured");
        Telegram.prototype.getMe = jest.fn(() => Promise.reject(error));
        new TelegramApi("mytoken", 1124, "http://uri").logInfo()
          .then(() => {
            expect(console.error).toHaveBeenCalledTimes(1);
            done();
          });
      });

      it("it should log in info", done => {
        const user = (new TelegramUser() as User);
        Telegram.prototype.getMe = Telegram.prototype.getMe = jest.fn(() => Promise.resolve(user));
        new TelegramApi("mytoken", 1124, "http://uri").logInfo()
          .then(() => {
            expect(console.info).toHaveBeenCalledTimes(1);
            done();
          });
      });
    });
  });
});

function assertProcessMessage(telegramUpdate: TelegramUpdate, expect: Function) {
  mockedAxios.post.mockImplementationOnce((url: string, data?: any, _config?) => {
    expect(url, data.message, data.isAuthenticated);
    return Promise.resolve(axiosResponse(new ResponseText("a_message")));
  });
  simulateReceiveMessage(telegramUpdate);
}

function simulatebotResponse(success: boolean) {
  const result = success ? Promise.resolve(axiosResponse(new ResponseText("Success"))) : Promise.reject(axiosError());
  mockedAxios.post.mockImplementationOnce(() => result);
}

function simulateReceiveMessage(telegramUpdate: TelegramUpdate) {
  Telegram.prototype.on = jest.fn().mockImplementationOnce((_intentFilter, callback) => {
    const update: Update = telegramUpdate;
    callback(update);
  });
}

